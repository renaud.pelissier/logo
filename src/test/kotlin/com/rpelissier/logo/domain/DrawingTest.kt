package com.rpelissier.logo.domain

import org.junit.Assert
import org.junit.Test

class DrawingTest {


    @Test
    fun testInitialCursorPosition() {
        var drawing = Drawing(Canvas(30))
        Assert.assertEquals(Coordinates(15, 15), drawing.cursorPosition)

        drawing = Drawing(Canvas(7))
        Assert.assertEquals(Coordinates(3, 3), drawing.cursorPosition)
    }

    @Test
    fun testInitialDirection() {
        var drawing = Drawing(Canvas(2))
        Assert.assertEquals(Direction.TOP, drawing.direction)
    }

    @Test
    fun testSteps() {
        val canvas = Canvas(2)

        val drawing = Drawing(canvas)

        Assert.assertEquals(Coordinates(1, 1), drawing.cursorPosition)
        drawing.brush = Brush.draw

        drawing.steps(1)

        Assert.assertEquals(Brush.draw.figure, canvas.getPoint(Coordinates(1, 1)))
        Assert.assertEquals(Coordinates(1, 0), drawing.cursorPosition)

        val expectedRendering =
            "" +
                    "╔══╗\r\n" +
                    "║  ║\r\n" +
                    "║ *║\r\n" +
                    "╚══╝\r\n"

        val rendering = CanvasRenderer(canvas).render()
        Assert.assertEquals(expectedRendering, rendering)
    }

    @Test
    fun testOneStepDefaultSettings() {
        val canvas = Canvas()
        val drawing = Drawing(canvas)

        drawing.steps(1)

        val rendering = drawing.render()

        val expectedRendering =
            "" +
                    "╔══════════════════════════════╗\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║               *              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "║                              ║\r\n" +
                    "╚══════════════════════════════╝\r\n"

        Assert.assertEquals(expectedRendering, rendering)
    }
}