package com.rpelissier.logo.domain

import org.junit.Assert
import org.junit.Test

class CanvasRendererTest {

    @Test
    fun testAxisOrientationAndBorders() {
        val canvas = Canvas(2)

        canvas.setPoint(Coordinates(0, 0), 'a')
        canvas.setPoint(Coordinates(1, 0), 'b')
        canvas.setPoint(Coordinates(0, 1), 'c')
        canvas.setPoint(Coordinates(1, 1), 'd')

        val expectedRendering =
            "" +
                    "╔══╗\r\n" +
                    "║ab║\r\n" +
                    "║cd║\r\n" +
                    "╚══╝\r\n"

        val rendering = CanvasRenderer(canvas).render()
        Assert.assertEquals(expectedRendering, rendering)
    }
}