package com.rpelissier.logo.domain

import org.junit.Assert
import org.junit.Test

class CanvasTest {

    @Test
    fun testBasicGetAndSet() {
        val canvas = Canvas(3)
        val coord = Coordinates(2, 2)
        val char = '*'
        canvas.setPoint(coord, char)
        Assert.assertEquals(canvas.getPoint(coord), char)
    }

    @Test
    fun testOutOfBounds() {
        val canvas = Canvas(3)

        try {
            canvas.getPoint(Coordinates(3, 2))
            Assert.fail("Should have thrown an Exception.")
        } catch (e: ArrayIndexOutOfBoundsException) {
        }

        try {
            canvas.getPoint(Coordinates(2, -1))
            Assert.fail("Should have thrown an Exception.")
        } catch (e: ArrayIndexOutOfBoundsException) {
        }
    }

}