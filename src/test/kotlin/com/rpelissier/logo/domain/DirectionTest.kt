package com.rpelissier.logo.domain

import org.junit.Assert
import org.junit.Test

class DirectionTest {
    @Test
    fun testOneStepLeftRight() {
        Assert.assertEquals(Direction.TOP_RIGHT, Direction.TOP.right(1))
        Assert.assertEquals(Direction.TOP_RIGHT, Direction.TOP.left(-1))

        Assert.assertEquals(Direction.TOP_LEFT, Direction.TOP.left(1))
        Assert.assertEquals(Direction.TOP_LEFT, Direction.TOP.right(-1))
    }

    @Test
    fun testMultipleSteps() {
        Assert.assertEquals(Direction.BOTTOM, Direction.TOP.right(4))

        Assert.assertEquals(Direction.BOTTOM, Direction.TOP.right(-4))

        Assert.assertEquals(Direction.TOP, Direction.TOP.right(8000))
    }
}