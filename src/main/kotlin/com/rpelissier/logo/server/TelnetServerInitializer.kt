package com.rpelissier.logo.server

import io.netty.buffer.Unpooled
import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import io.netty.handler.ssl.SslContext

class TelnetServerInitializer(private val sslCtx: SslContext?, private val responderFactory: () -> TelnetResponder) :
    ChannelInitializer<SocketChannel>() {

    public override fun initChannel(ch: SocketChannel) {
        val pipeline = ch.pipeline()

        if (sslCtx != null) {
            pipeline.addLast(sslCtx.newHandler(ch.alloc()))
        }

        // Add the text line codec combination first,
        val delimiter = TelnetServer.delimiter.toByteArray()
        pipeline.addLast(DelimiterBasedFrameDecoder(8192, Unpooled.wrappedBuffer(delimiter)))

        // the encoder and decoder are static as these are sharable
        pipeline.addLast(DECODER)
        pipeline.addLast(ENCODER)

        // and then business logic.
        pipeline.addLast(TelnetServerHandler(responderFactory()))
    }

    companion object {

        private val DECODER = StringDecoder()
        private val ENCODER = StringEncoder()
    }
}