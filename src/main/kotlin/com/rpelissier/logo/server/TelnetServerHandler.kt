package com.rpelissier.logo.server

import io.netty.channel.ChannelFutureListener
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler


class TelnetServerHandler(private val responder: TelnetResponder, private val withWelcomeMessage: Boolean = false) :
    SimpleChannelInboundHandler<String>() {

    override fun channelActive(ctx: ChannelHandlerContext?) {
        ctx!!.write(TelnetServer.helloMessage)
        ctx!!.flush()
    }

    override fun channelRead0(ctx: ChannelHandlerContext, request: String) {
        // Generate and write a response.
        val response: String
        if (TelnetServer.exitCommand == request.toLowerCase()) {
            ctx.flush()
            ctx.close()
            return
        }

        val businessResponse = responder.respond(request)
        if (businessResponse.isEmpty() || businessResponse == TelnetResponder.BAD_REQUEST) {
            return
        } else {
            response = businessResponse + TelnetServer.delimiter
        }


        // We do not need to write a ChannelBuffer here.
        // We know the encoder inserted at TelnetPipelineFactory will do the conversion.
        ctx.write(response)
        ctx.flush()
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext) {
        ctx.flush()
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        cause.printStackTrace()
        ctx.close()
    }
}