package com.rpelissier.logo.server

interface TelnetResponder {

    /**
     * Provide a response or return BAD_REQUEST
     */
    fun respond(request: String): String

    companion object {

        val BAD_REQUEST = "BAD_REQUEST"
    }

    class EchoResponder : TelnetResponder {
        override fun respond(request: String): String {
            if (request.isEmpty()) {
                return "Please say something.\r\n"
            } else {
                return "Did you say '$request'?\r\n"
            }
        }
    }
}