package com.rpelissier.logo.server

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.ssl.SslContext
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.util.SelfSignedCertificate

class TelnetServer(private val responderFactory: () -> TelnetResponder) : Runnable {

    private val SSL = System.getProperty("ssl") != null
    private val PORT = Integer.parseInt(System.getProperty("port", if (SSL) "8125" else "8124"))

    companion object {
        val delimiter = "\r\n"
        val exitCommand = "quit"
        val helloMessage = "hello$delimiter"

        @JvmStatic
        fun main(args: Array<String>) {
            val server = TelnetServer({ TelnetResponder.EchoResponder() })
            server.run()
        }
    }

    override fun run() {
        // Configure SSL.
        val sslCtx: SslContext?
        if (SSL) {
            val ssc = SelfSignedCertificate()
            sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build()
        } else {
            sslCtx = null
        }

        val bossGroup = NioEventLoopGroup(1)
        val workerGroup = NioEventLoopGroup()
        try {
            val b = ServerBootstrap()
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel::class.java)
                .childHandler(TelnetServerInitializer(sslCtx, responderFactory))

            b.bind(PORT).sync().channel().closeFuture().sync()
        } finally {
            bossGroup.shutdownGracefully()
            workerGroup.shutdownGracefully()
        }
    }
}
