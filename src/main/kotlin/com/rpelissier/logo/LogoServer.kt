package com.rpelissier.logo

import com.rpelissier.logo.server.TelnetServer

object LogoServer {

    @JvmStatic
    fun main(args: Array<String>) {
        val server = TelnetServer({ LogoSession() })
        server.run()
    }
}