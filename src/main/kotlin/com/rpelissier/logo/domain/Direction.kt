package com.rpelissier.logo.domain

enum class Direction {

    TOP,
    TOP_RIGHT,
    RIGHT,
    BOTTOM_RIGHT,
    BOTTOM,
    BOTTOM_LEFT,
    LEFT,
    TOP_LEFT;


    fun right(n: Int): Direction {
        var newOrdinal = (ordinal + n) % values().size
        if (newOrdinal < 0) newOrdinal = newOrdinal + values().size
        return Direction.values()[newOrdinal]
    }

    fun left(n: Int): Direction {
        return right(-n)
    }
}