package com.rpelissier.logo.domain

enum class Brush(val figure: Char) {
    hover(' '),
    draw('*'),
    eraser(' ')
}