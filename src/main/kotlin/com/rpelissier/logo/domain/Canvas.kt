package com.rpelissier.logo.domain

class Canvas(val size: Int = 30, private val defaultValue: Char = ' ') {

    val rows = arrayOfNulls<CharArray>(size)

    fun getPoint(c: Coordinates): Char {
        return getOrCreateRow(c.y)[c.x]
    }

    fun setPoint(c: Coordinates, value: Char) {
        val row = getOrCreateRow(c.y)
        row[c.x] = value
    }

    fun row(y: Int): CharArray {
        return getOrCreateRow(y)
    }

    private fun getOrCreateRow(y: Int): CharArray {
        var row = rows[y]
        if (row == null) {
            row = CharArray(size, { _ -> defaultValue })
            rows[y] = row
        }
        return row
    }
}