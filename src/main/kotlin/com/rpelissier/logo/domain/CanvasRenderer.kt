package com.rpelissier.logo.domain

class CanvasRenderer(private val canvas: Canvas) {
    private val borderTopBottomChar = '═'
    private val borderTopLeftChar = '╔'
    private val borderTopRightChar = '╗'
    private val borderBottomLeftChar = '╚'
    private val borderBottomRightChar = '╝'
    private val leftRightBorderChar = '║'


    /**
     * print the current canvas.
     */
    fun render(): String {
        val stringBuilder = StringBuilder()
        for (y in -1..canvas.size) {
            when (y) {
                -1 -> stringBuilder.append(topRow())
                canvas.size -> stringBuilder.append(bottomRow())
                else -> {
                    val row = canvas.row(y)
                    stringBuilder.append(leftRightBorderChar)
                    stringBuilder.append(row)
                    stringBuilder.append(leftRightBorderChar)
                }
            }
            stringBuilder.append("\r\n")
        }
        return stringBuilder.toString()
    }

    private fun topRow(): CharArray {
        val array = CharArray(canvas.size + 2, { _ -> borderTopBottomChar })
        array[0] = borderTopLeftChar
        array[canvas.size + 1] = borderTopRightChar
        return array
    }

    private fun bottomRow(): CharArray {
        val array = CharArray(canvas.size + 2, { _ -> borderTopBottomChar })
        array[0] = borderBottomLeftChar
        array[canvas.size + 1] = borderBottomRightChar
        return array
    }
}