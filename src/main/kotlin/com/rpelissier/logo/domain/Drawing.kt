package com.rpelissier.logo.domain

class Drawing(
    var canvas: Canvas = Canvas(),
    var cursorPosition: Coordinates = Coordinates(canvas.size / 2, canvas.size / 2),
    var brush: Brush = Brush.draw,
    var direction: Direction = Direction.TOP
) {

    val renderer = CanvasRenderer(canvas)

    /**
     * repeat the step() operation multiple times
     */
    fun steps(n: Int) {
        repeat(n, { step() })
    }

    /**
     * modify the current position and move the cursor 1 step in the current direction
     */
    private fun step() {
        modify()
        move()
    }

    private fun modify() {
        if (brush != Brush.hover) canvas.setPoint(cursorPosition, brush.figure)
    }

    private fun move() {
        val newCursorPosition = cursorPosition.moveTo(direction)
        if (withinCanvas(newCursorPosition)) {
            cursorPosition = newCursorPosition
        }
    }

    private fun withinCanvas(c: Coordinates): Boolean {
        return c.x >= 0 || c.x < canvas.size || c.y >= 0 || c.y < canvas.size
    }

    /**
     * change the direction anti-clockwise
     */
    fun left(n: Int) {
        direction = direction.left(n)
    }

    /**
     * change the direction clockwise
     */
    fun right(n: Int) {
        direction = direction.right(n)
    }


    /**
     * print the current canvas.
     */
    fun render(): String {
        return renderer.render()
    }

    /**
     * erase the current canvas, while keeping the current cursor and direction.
     */
    fun clear() {
        canvas = Canvas()
    }

}