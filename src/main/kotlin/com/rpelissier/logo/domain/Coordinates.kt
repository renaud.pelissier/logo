package com.rpelissier.logo.domain

/**
 * The top left coordinate is (0, 0). X increases when you move to the right, while Y increase when you move to the bottom.
 *  --------> (X)
 *  |
 *  |
 *  |
 *  v
 *  (Y)
 */
data class Coordinates(val x: Int, val y: Int) {


    fun moveTo(direction: Direction): Coordinates {


        var xIncr = when (direction) {
            Direction.TOP_RIGHT,
            Direction.BOTTOM_RIGHT,
            Direction.RIGHT -> 1
            Direction.BOTTOM,
            Direction.TOP -> 0
            Direction.BOTTOM_LEFT,
            Direction.LEFT,
            Direction.TOP_LEFT -> -1
        }

        var yIncr = when (direction) {
            Direction.BOTTOM,
            Direction.BOTTOM_LEFT,
            Direction.BOTTOM_RIGHT -> 1
            Direction.LEFT,
            Direction.RIGHT -> 0
            Direction.TOP_LEFT,
            Direction.TOP_RIGHT,
            Direction.TOP -> -1
        }

        return Coordinates(x + xIncr, y + yIncr)
    }

    override fun toString(): String {
        return "($x,$y)"
    }
}