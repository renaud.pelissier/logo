package com.rpelissier.logo

import com.rpelissier.logo.domain.Brush
import com.rpelissier.logo.domain.Drawing
import com.rpelissier.logo.server.TelnetResponder

class LogoSession : TelnetResponder {

    val drawing = Drawing()

    override fun respond(request: String): String {
        try {
            val (cmd, n) = parse(request)
            return respond(cmd, n)
        } catch (e: Exception) {
            e.printStackTrace()
            return TelnetResponder.BAD_REQUEST
        }
    }

    private fun respond(cmd: String, n: Int): String {
        var response = ""
        when (cmd) {
            "hover", "draw", "eraser" -> {
                drawing.brush = Brush.valueOf(cmd)
            }
            "steps" -> {
                drawing.steps(n)
            }
            "left" -> {
                drawing.left(n)
            }
            "right" -> {
                drawing.right(n)
            }
            "coord" -> {
                response = drawing.cursorPosition.toString()
            }
            "render" -> {
                response = drawing.render()
            }
            "clear" -> {
                drawing.clear()
            }
            else -> response = TelnetResponder.BAD_REQUEST
        }
        return response
    }

    private fun parse(request: String): Pair<String, Int> {
        val cleanedRequest = request.trim()
        var n = 1
        val parts = cleanedRequest.split(" ")
        val cmd = parts[0]
        if (parts.size > 1) {
            n = parts[1].toInt()
        }
        return cmd to n
    }
}