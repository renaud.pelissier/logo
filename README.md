# Logo server implementation

## Introduction

The purpose is to solve this [exercise](TODO.md) from Front hiring team.

The implementation is in Kotlin and relying on the great [Netty API](https://netty.io/).
It will produce a JAR file containing an executable end point.

The build tool is [Gradle](https://gradle.org/).

The unit test framework is [Junit](https://junit.org/junit4/).

## How to use this project

Java 8+ is a prerequisite.

To build and start the server we will use the [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) bundled with this project :
``` shell script
cd $PROJECT_DIR
chmod 550 gradlew
./gradlew build
./gradlew run
```

Press ^C to exit.

## How to play with the server

Just install the `telnet` tool.

On MacOS:
``` shell script
brew install telnet
```

Then use it.  
TIPS: make sure the line ending is Windows style \r\n when copying into the terminal

``` shell script
telnet 127.0.0.1 8124
steps 1
render
quit
```

a more advanced drawing 


``` shell script
telnet 127.0.0.1 8124
hover
steps 3
left 2
steps 6
draw
right 3
steps 6
right 2
steps 12
right 3
steps 24
right 3
steps 6
right
steps 12
right 3
steps 6
right 2
steps 6
render
```
